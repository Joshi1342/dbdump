import logging, mysql.connector, os, datetime, time, shutil, configparser, fnmatch, argparse, sys
from pathlib import Path
from datetime import datetime, timedelta

#-- Help

parser = argparse.ArgumentParser(description = 'Creates dumps and administrates old dumps')
parser.add_argument('Start', help = 'Starts the whole process. Must have a value assigned to it. If -1- is assigned to the parameter, it will start the main-method. Otherwise, it will ignore the main-method.')
parser.add_argument('--administration', help = 'Starts the administration process. If -1- is assigned to the parameter, it will start the administration classes. Otherwise, it will be ignored. Not necessary if you assigned the "start" parameter -1-.')
args = parser.parse_args()

config = configparser.ConfigParser()
config.read("/mnt/c/Users/jz_da/Documents/Wichtig.ini")

#-- Global variable

datum = datetime.now().strftime('%H-%M-%S_%d-%m-%Y')

base_dir = config["wichtig"]["base_dir"]
unittest_dir = config['wichtig']['unittest_dir']
unittest_file = config['wichtig']['unittest_file']
Logdir_name = config["wichtig"]["Logdir_name"]
Logdir = base_dir +  Logdir_name
Logfile = Path(Logdir + "/log-{}.txt".format(datum))

#-- Create directories

class directory:

    def __init__(self, path, name):
        self.path = path
        self.name = name

    def create(self):
        target_dir = self.path + self.name
        if not os.path.exists(target_dir):
            os.mkdir(target_dir)

#-- Logging class

class Logging_conf:

    def __init__(self, filemode, file_format, Logfile_name):

        #-- Create log directory
        logging_dir = directory(base_dir, '/logs')
        logging_dir.create()

        self.filemode = filemode
        self.file_format = file_format
        self.Logfile_name = Logfile_name

        if self.Logfile_name.is_file():
            pass
        else:
            open(self.Logfile_name, mode = 'a')

        logging.basicConfig(filename = self.Logfile_name, filemode = self.filemode, format = self.file_format, level = logging.INFO)

#-- initialise logging

Logging_conf('a', '%(asctime)s - %(levelname)s - %(message)s', Logfile)

#-- Dumps

class sqldump:

    def __init__(self, user, password, database, path, dumpname):
        self.user = user
        self.password = password
        self.database = database
        self.path = path
        self.dumpname = dumpname

    def create_dump(self):
        try:
            os.system("mysqldump -u{} -p{} {} | gzip > {}/{}".format(self.user, self.password, self.database, self.path, self.dumpname))
            logging.info('Dump {} created: {}/{}'.format(self.dumpname, self.path, self.dumpname))
        except:
            logging.error('Could not create DB Dump!')

    def delete_dump(self):
        try:
            os.system("rm -f {}".format(self.dumpname))
        except:
            logging.error('Could not remove dump!')

#-- Move class

class move_files:

    def __init__(self, source, destination):
        self.source = source
        self.destination = destination

        shutil.move(source, destination)
        logging.info('File {} moved to {}'.format(source, destination))

#-- Dump administration

class dump_administration:
    
    def __init__(self, topdown, mode):
        self.topdown = topdown
        self.mode = mode

        if self.mode == 0:
            directory = '/dump'
            days = 15
            delta = 14
            logging.info('Check if dump creation is older than 14 days')

        if self.mode == 1:
            directory = '/archive'
            days = 29
            delta = 28
            logging.info('Check if dump creation is older than 28 days')

        date_now = datetime.now()
        date_delta = (date_now - timedelta(days = delta))

        for root, dirs, files in os.walk(base_dir + directory, topdown = self.topdown):
            for name in files:
                path = os.path.join(root, name)
                date_file = datetime.fromtimestamp(os.path.getctime(path))
                delta_days = (date_delta - date_file).days
                if delta_days > days:
                    if self.mode == 0:
                        move_files(path, base_dir + '/archive')
                        logging.info('Moving dump {} to archive folder'.format(name))
                    if self.mode == 1:
                        if fnmatch.fnmatch(Path, '*.sql.12-00-*'):
                            dump = sqldump(dumpname = path)
                            dump.delete_dump()
                            logging.info('Removing {}'.format(name))

def main():

    #-- Create directories
    dump_dir = directory(base_dir, '/dump')
    archive_dir = directory(base_dir, '/archive')

    dump_dir.create()
    archive_dir.create()

    #-- Make dump
    dump = sqldump(user = config["Login"]["Username"], password = config["Login"]["Password"], database = config["Login"]["DatabaseName"], path = base_dir + '/dump', dumpname = 'dump.sql.{}.gz'.format(datum))
    dump.create_dump()

    #-- Move files if creation date > 15 days
    dump_administration(True, 0)

    #-- Delete files if creation time is 12:00 and > 29 days
    dump_administration(True, 1)

def conn():

    db = mysql.connector.connect(
        host = config['Login']['Host'],
        user = config['Login']['Username'],
        password = config['Login']['Password'],
        database = config['Login']['DatabaseName'],
        auth_plugin = 'mysql_native_password'
    )

def unit_test():


    #-- Testing DB Connection
    logging.info('1st Unittest: testing DB connection..')
    try:
        conn()
        logging.info('DB connection successfully established!')
    except:
        logging.error('DB connection could not be established. Aborting script')
        sys.exit()

    #-- Testing File and Directory creation
    logging.info('2nd Unittest: Creating files and directories..')
    try:
        os.makedirs(unittest_dir)
        open(base_dir + unittest_file, mode = 'a')
        logging.info('Creation of files and directories was successful!')
    except:
        logging.error('Could not create files and directories. Aborting script')
        sys.exit()

    #-- Testing moving of files
    logging.info('3st Unittest: moving files..')
    try:
        move_files(base_dir + unittest_file, unittest_dir)
        logging.info('Moved files successfully!')
    except:
        logging.error('Could not move files. Aborting script')
        sys.exit()

    #-- Testing Removing of test-files and directories
    logging.info('4nd Unittest: Removing files and directories..')
    try:
        os.remove(unittest_dir + unittest_file)
        shutil.rmtree(unittest_dir)
        logging.info('Removing test file and directory was successful! Starting main Skript')
    except:
        logging.error('Could not remove the test-directory. Aborting script')
        sys.exit()

    #-- Checking parameters
    try:
        if args.Start == '1':
            #-- Starting main-method
            logging.info('Unittest looks good and start parameter == 1. Starting main-method..')
            print('Unittest looks good and start parameter == 1. Starting main-method..')
            main()
        else:
            pass
    except:
        logging.error('Could not start the main method!')

    try:
        if args.administration == '1':
            #-- Starting administration-method
            logging.info('Unittest looks good and administration parameter == 1. Starting administration-method..')
            print('Unittest looks good and administration parameter == 1. Starting administration-method..')
            dump_administration(topdown=True, mode=1)
            dump_administration(topdown=True, mode=0)
        else:
            pass
    except:
        logging.error('Could not start the administration method! There is an error between lines 94 - 128.')

unit_test()